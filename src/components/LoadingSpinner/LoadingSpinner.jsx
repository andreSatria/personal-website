import React from 'react';
import { LoadingSpinnerContainer, } from './style';

function LoadingSpinner() {
    return (
        <LoadingSpinnerContainer>
            <div className="Loader__wrapper">
                <span className="loader triangle"/>
                <span className="loader triangle2"/>
                <span className="loader triangle3"/>
            </div>
        </LoadingSpinnerContainer>
    );
}

export default LoadingSpinner;
