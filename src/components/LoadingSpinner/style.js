import styled from "styled-components";

export const LoadingSpinnerContainer = styled.div`
  max-height: 50px;
  
 .Loader__wrapper {
  width: 50px;
  height: 50px;
  position:relative;
 }
 
.loader {
    --path: white;
    --dot: white;
    --duration: 3.5s;
    width: 44px;
    height: 44px;
    &:before {
        content: '';
        width: 7px;
        height: 7px;
        border-radius: 50%;
        position: absolute;
        display: block;
        background: white;
        top: 37px;
        left: 19px;
        transform: translate(0px, 0px);
    }
    
    &.triangle {
        width: 48px;
        &:before {
            left: 21px;
            transform: translate(0px, 0px);
            animation: dotTriangle3 3.5s cubic-bezier(0.785, 0.135, 0.15, 0.86) infinite;
        }
    }
    
    &.triangle2 {
        width: 48px;
        &:before {
            left: 21px;
            transform: translate(-10px, -18px);
            animation: dotTriangle 3.5s cubic-bezier(0.785, 0.135, 0.15, 0.86) infinite;
        }
    }
    
    &.triangle3 {
        width: 48px;
        &:before {
            left: 21px;
            transform: translate(10px, -18px);
            animation: dotTriangle2 3.5s cubic-bezier(0.785, 0.135, 0.15, 0.86) infinite;
        }
    }    

    @keyframes dotTriangle {
        33% {
            transform: translate(0, 0);
        }
        66% {
            transform: translate(10px, -18px);
        }
        100% {
            transform: translate(-10px, -18px);
        }
    }
    
        @keyframes dotTriangle2 {
        33% {
            transform: translate(-10px, -18px);
        }
        66% {
            transform: translate(0, 0);
        }
        100% {
            transform: translate(10px, -18px);
        }
    }
    
    @keyframes dotTriangle3 {
        33% {
            transform: translate(10px, -18px);
        }
        66% {
            transform: translate(-10px, -18px);
        }
        100% {
            transform: translate(0, 0);
        }
    }
 }

`;
