import React from 'react';

import { Switch, Route, BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";

import { theme } from "./theme";
import { AppContainer } from './style';
import { routes } from "./routes";

import './App.css'

function App() {
  const pages = routes.map((route, int) => <Route
        component={route.component}
        exact={route.exact}
        path={route.path}
        key={int}
    />);

  return (
      <ThemeProvider theme={theme}>
        <AppContainer>
          <BrowserRouter basename={process.env.PUBLIC_URL}>
            <Switch>
              {pages}
            </Switch>
          </BrowserRouter>
        </AppContainer>
      </ThemeProvider>
  );
}

export default App;
