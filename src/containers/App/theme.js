export const theme = {
    'border': {
        'borderRadius': '0.25rem',
        'shadowColor': 'rgba(36,37,38,0.13)',
        'shadowConfig': '0 0.25rem 1rem'
    },
    'colors': {
        'ashWhite': '#F3F3F3',
        'black': '#000000',
        'blue': '#6287A2',
        'brown': '#CE8F5A',
        'darkBlue': '#005A72',
        'darkGrey': '#5c5c5c',
        'grey': '#EAEAEA',
        'inherit': 'inherit',
        'ivory': '#F9FAFB',
        'lightBlue': 'rgba(94, 192, 202, 0.3)',
        'lightBrown': '#F0D19A',
        'lightGrey': '#999999',
        'lightPink': 'rgba(238, 209, 152, 0.3)',
        'red': '#CC4040',
        'teal': '#80C8BC',
        'white': '#FFFFFF',
    }
};
