import HomePage from "../HomePage/HomePage";
import ComingSoonPage from "../ComingSoonPage/ComingSoonPage";

export const routes = [
    {
        component: ComingSoonPage,
        exact: true,
        path: "/",
    },
    {
        component: HomePage,
        exact: true,
        path: "/home",
    }
]
