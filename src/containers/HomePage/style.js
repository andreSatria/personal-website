import styled from "styled-components";

export const HomePageContainer = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: black;
  
  h1 {
    color: white;
    font-family: "Futura PT Demi";
  }
  
`;

