import React from 'react';
import {HomePageContainer} from "./style";
import LoadingSpinner from "../../components/LoadingSpinner/LoadingSpinner";


function HomePage() {
    return (
        <HomePageContainer>
            <LoadingSpinner/>
        </HomePageContainer>
    );
}

export default HomePage;
