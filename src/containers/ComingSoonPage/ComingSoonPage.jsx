import React from 'react';
import {ComingSoonPageContainer} from "./style";
import LoadingSpinner from "../../components/LoadingSpinner/LoadingSpinner";


function ComingSoonPage() {
    return (
        <ComingSoonPageContainer>
            <div className="Spinner__wrapper">
                <LoadingSpinner/>
            </div>
            <div className="Soon__wrapper">
                <p>soon.</p>
            </div>
        </ComingSoonPageContainer>
    );
}

export default ComingSoonPage;
