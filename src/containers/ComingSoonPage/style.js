import styled from "styled-components";

export const ComingSoonPageContainer = styled.div`
  width: 100vw;
  height: 100vh;
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: black;
  
  .Spinner__wrapper {
    display: flex;
    align-self: center;
    justify-content: center;
    width:50px;
    height:50px;
  }
  
  .Soon__wrapper {
     p {
        color: white;
        font-size: 1.65rem;
        margin:0;
        font-family: "Futura PT Demi";
      }
  }
  
`;
